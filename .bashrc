export EDITOR="vim"
export BROWSER="firefox"
export NO_AT_BRIDGE=1
export ANDROID_HOME="$HOME/Android/Sdk/"
export TERM="xterm-256color"

alias cordr="cordova run browser -- --target=firefox"

alias ym="youtube-dl -x --audio-format best"
alias pm="mpv http://radio.2f30.org:8000/live.mp3"
alias podcast="mpv http://vdl.stream-lat.org:8000/voixdulat_mp3"
alias lxca="lxc-attach --clear-env -n"
alias grabd="ffmpeg -video_size 1920x1080 -framerate 25 -f x11grab -i :0.0 $HOME/videos/screencasts/$(date +'%Y-%m-%d-%H%M%S').mp4"
pub2pi() { newname=$(basename "$1" | sed -r 's/[ ^I]/-/g'); scp "$1" "turing:/srv/www/perso/misc2/$newname" && echo "http://perso.simonthoby.tk/misc2/$newname" ; }

export XDG_RUNTIME_DIR=~/.config/
export XDG_DATA_HOME=~/.local/share/
# Run ssh-agent on real tty only
tty | grep tty > /dev/null && eval $(ssh-agent)
pb () {
  curl -F "c=@${1:--}" https://ptpb.pw/
}
export GOPATH=$HOME/go/
export PATH="/usr/lib/ccache/bin:$HOME/prog/bin:/usr/local/bin:/bin:/usr/bin:/usr/sbin:/sbin:$HOME/.cargo/bin:$HOME/bin"
export MANPATH="/home/nightmared/prog/share/man:/usr/share/man"
export LD_LIBRARY_PATH="$HOME/prog/lib:/usr/local/lib:/usr/lib"
export PKG_CONFIG_PATH="/home/nightmared/prog/lib/pkgconfig"
export PS1="[\u \W]$ "
export USE_CCACHE=1

export GTK_CSD=0

#export PYTHONPATH="/usr/lib/python34.zip:/usr/lib/python3.4:/usr/lib/python3.4/plat-linux:/usr/lib/python3.4/lib-dynload:/usr/lib/python3.4/site-packages:$HOME/prog/lib/python3.4/site-packages/"

#export JAVA_HOME=/usr/lib/jvm/openjdk/

#export ECORE_EVAS_ENGINE=wayland_egl
#export ELM_DISPLAY=wl
#export ELM_ACCEL=opengl
#export ELM_ENGINE=wayland_egl
#export GDK_BACKEND=wayland
#eval `opam config env`
