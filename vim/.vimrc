set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
"Plugin 'Valloric/YouCompleteMe'
Plugin 'rust-lang/rust.vim'
"Plugin 'scrooloose/syntastic'
"Plugin 'vim-airline/vim-airline'
"Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Plugin 'wincent/command-t'

call vundle#end()            " required
filetype plugin indent on    " required
syntax on


"let g:ycm_rust_src_path = '/home/t0b1nux/development/rust/rust/src/'
"let g:ftplugin_rust_source_path = '/home/t0b1nux/development/rust/rust/src/'
"let g:rustfmt_autosave = 1
"let g:ycm_python_binary_path = '/usr/bin/python3'

set pastetoggle=<F7>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set splitbelow
set splitright



let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"

set laststatus=2

let g:molokai_original = 1

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = '/usr/bin/python3'
let g:syntastic_ocaml_checkers = ['merlin']


so ~/.vim/colors/molokai.vim

set encoding=utf-8
set expandtab
set tabstop=4 shiftwidth=4 expandtab
set number
set relativenumber

map <F5> :tabprevious<CR>
map <F6> :tabnext<CR>
map <C-n> :NERDTreeToggle<CR>
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>
let g:NERDTreeDirArrowExpandable = '>'
let g:NERDTreeDirArrowCollapsible = '-'

map <C-t> :CommandT<CR>
